function trials = GL_pilot(subject)

% Navon tasks are modelled after:
% Gable, P. A., & Harmon-Jones, E. (2011). Attentional consequences of pregoal and postgoal positive affects. Emotion, 11(6), 1358�1367. https://doi.org/10.1037/a0025611
% Dale, G., & Arnell, K. M. (2010). Individual differences in dispositional focus of attention predict attentional blink magnitude. Attention, Perception, & Psychophysics, 72(3), 602�606. https://doi.org/10.3758/APP.72.3.602


%% SUBJECT %%
    if nargin < 1
       subject = inputdlg('Sisesta katseisiku kood');
       list = {'1','2'};
       mapping = listdlg('ListString', list, 'PromptString', 'Response mapping:', 'SelectionMode', 'Single', 'ListSize', [120 80]); 
       list = {'global','local'};
       order = listdlg('ListString', list, 'PromptString', 'Induction order:', 'SelectionMode', 'Single', 'ListSize', [120 80]);  
    end

%% PARAMETERS %%
    prm.testRun = 0; 
    prm.order = order;
    prm.trls.induction = 80;                                                %4 different stimuli
    prm.trls.assessment = 80;                                               %8 different stimuli
    prm.trls.inductionPractice = 8;
    prm.trls.assessmentPractice = 8;
    prm.mapping = mapping;                                                 % 1: T left & H right; 2: H right & T left     
    
    col.bkg =               [120 120 120];                                 %background color    
    col.obj =               [30 30 30];                                    
    col.txt =               [30 30 30];   
    col.mid =               [75 75 75];
    
    prm.respDevice = {'mouse', 1, 3}; 
    prm.ITI = 1;
    prm.fix = 0.5;
    prm.txtFont =           'Helvetica';
    prm.txtSize = 18;                                                      

    prm.target = struct(...
    'name', {'gHlT','gHlH','gTlH','gTlT'},...                                                
    'format', {'pic','pic','pic','pic'});

    % failis�steem %
    root = fileparts(mfilename('fullpath'));root = [root '\']; 
    dataFile = [root 'data\data.txt'];
    dataFileSlf = [root 'data\dataslf.txt'];
    prm.loc.induction = [root 'stmuliHallid\induction\'];
    prm.loc.assessment = [root 'stmuliHallid\assessment\'];
    prm.loc.intros = [root 'introsHallid\'];

    if prm.testRun == 1
        prm.ITI = 0.1;
        prm.fix = 0.1;
    end
    
%% TEXTS %%
    txt.intro = [
        'Tere tulemast katsesse!',...
        '\n\nSelle katse jooksul tuleb Sul teha kolme erinevat �lesannet.',... 
        'Katse on jaotatud kolme blokki. Blokkide vahel on pausid, ',...
        'kus saad tagasisidet oma soorituse kohta.',...
        '\nEnne p�ris katse juurde asumist saad k�iki �lesandeid ka harjutada. ',...
        '\n\n\nPea meeles, et iga �lesande puhul on oluline teha v�imalikult v�he vigu ja',... 
        'vastata v�imalikult kiiresti esmamuljest l�htuvalt.',... 
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.feedback = [
        'Katse on l�ppenud!',...
        '\n\nSinu keskmine reaktsiooniaeg: '];
    
    txt.end = ['Ait�h!'];

    txt.global = [
        '�lesanne: missugune on stiimuli globaalne kuju?',... 
        '\n\n�ige vastuse andmiseks vajuta vastavat hiireklahvi:',...
        '\n\n�H� puhul paremat ja �T� puhul vasakut.',...
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];

    txt.global1 = [
        '�lesanne: missugune on stiimuli globaalne kuju?',...
        '\n\n�ige vastuse andmiseks vajuta vastavat hiireklahvi:',...
        '\n�H� puhul vasakut ja �T� puhul paremat.',...
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.local = [
        '�lessanne: Millest stiimul koosneb?',... 
        '\n\n�ige vastuse andmiseks vajuta vastavat hiireklahvi:',...
        '\n\n�H� puhul paremat ja �T� puhul vasakut.',...
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.local1 = [
        '�lessanne: Millest stiimul koosneb?',...
        '\n\n�ige vastuse andmiseks vajuta vastavat hiireklahvi:',...
        '\n\n�H� puhul vasakut ja �T� puhul paremat.',...
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];  
    
    txt.assessment = [
        '�lesanne: Kas �T� v�i �H�?',... 
        '\n\nVajuta vasakut klahvi kui n�ed �T� t�hte.'...
        '\n\nVajuta paremat klahvi kui n�ed �H� t�hte.',...
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.assessment1 = [
        '�lesanne: Kas �T� v�i �H�?',... 
        '\n\nVajuta paremat klahvi kui n�ed �T� t�hte.'...
        '\n\nVajuta vasakut klahvi kui n�ed �H� t�hte.',... 
        'n\n\nPalun vasta v�imalikult kiiresti oma esmamuljest',...
        'l�htuvalt ja �rita teha v�imalikult v�he vigu.',...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.scalePractice = [
        'Esmalt tutvustame Sulle katse k�igus kasutatavaid k�simuse skaalasid.',...
        '\n\n Vajuta hiireklahvi, et neid proovida.'];

    txt.scaleReal = [
        'N��d kirjelda palun proovitud skaaladega oma praeguse hetke meeleolu.'];
    
    txt.Iblock.start1 = [
        'Algab esimene blokk.',...
        '\n\n See blokk koosneb �hest �lesandest.'...
        '\n '...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
        
    txt.Iblock.start2 = [
        'Algab esimene blokk.',...
        '\n\n See blokk koosneb kahest �lesandest.'...
        '\n '...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
    txt.Iblock.end = [
        'Esimene blokk on l�bi.',...
        '\n\nSinu keskmine reaktsiooniaeg oli: '];
    
   txt.IIblock.start1 = [                                                   % mapping 1
        'Algab teine blokk.',...
        '\n\n See blokk koosneb kahest �lesandest.'...
        '\n '...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
   txt.IIblock.start2 = [                                                   % mapping 2    
        'Algab teine blokk.',...
        '\n\n Selles bloksi tuleb Sul teha kaks �lesannet.',...
        '\n '...
        '\nJ�tkamiseks vajuta hiireklahvi.'];
    
   txt.IIblock.end =  [
        'Teine blokk on l�bi.',...
         '\n\nSinu keskmine reaktsiooniaeg oli: '];
    
    txt.IIIblock.start1 = [
        'Algab kolmas blokk',...
        '\n\n Selles blokis tuleb Sul teha kaks �lesannet'];
    txt.IIIblock.start2 = [
        'Algab kolmas blokk',...
        '\n\n Selles blokis tuleb Sul teha kaks �lesannet'];
    
   txt.IIIblock.end = [
        'Kolmas blokk on l�bi.',...
         '\n\nSinu keskmine reaktsiooniaeg oli: '];
     
%% RIISTVARA %%
    %Screen('Preference', 'SkipSyncTests', 1);
    KbName('UnifyKeyNames');
    nScr = Screen('Screens'); iScr = max(nScr);                            % v�tab k�rgeima ekraani numbri (kahe display puhul teise)
    Screen('Resolution', iScr, 1280, 800, 60, 32);
    [w, rect] = Screen('OpenWindow', iScr, [], []);                        % avab graafikaakna (w - akna handle, rect - selle suurus)
    [cx, cy] = RectCenter(rect);                                           % ekraani keskpunkti koordinaadid
    %jitter = Screen('GetFlipInterval', w)/2;                              % see tuleks Flip aegadest maha lahutada
    %Screen('Preference', 'ConserveVRAM', 2);
    prio = MaxPriority(w); Priority(prio);                                 % seab Matlabi protsessi prioriteedi windows'i jaoks k�rgeimaks
    HideCursor;                                                            % kursori peitmine

%% STIIMULID %%
    % particular location of the screen.
    baseRect = [0 0 100 100];
    % Center the rectangle on the centre of the screen using fractional pixel values.
    % For help see: CenterRectOnPointd
    centeredRect = CenterRectOnPointd(baseRect, cx, cy);
        
% FIXATION CROSS
    [prm.txtSize, ls] = deal(round(rect(4)*0.0225));                        % stiimuli suurusi muutev konstant
    locRect = [cx-4*ls cy-4*ls cx+1+4*ls cy+1+4*ls]; 
    inRect = round([cx-3.8*ls cy-3.8*ls cx+1+3.8*ls cy+1+3.8*ls]);
    
    width = round(ls/5); if width/2~=round(width/2); 
    width = width + 1; end; if width > 7; width = 7; end                    % joonte paksus (muudetakse vajadusel paarisarvuliseks)
    
    Screen('FillRect', w, col.bkg); Screen('DrawLines', w, [cx-ls cy; cx+ls cy; cx cy-ls; cx cy+ls]',width,col.mid);    % fiksatsioonirist
    t = Screen('GetImage', w, locRect, 'drawBuffer'); stim.fix = Screen('MakeTexture',w, t);  
    
    
%% TRIALITE DEFINEERIMINE %%
    stimuliInduction = {'gHlT.jpg', 'gHlH.jpg', 'gTlH.jpg' ,'gTlT.jpg'};
    stimuliAssessment = {'AgHlF.jpg', 'AgHlL.jpg', 'AgTlF.jpg' ,'AgTlL.jpg','AgFlH.jpg', 'AgFlT.jpg', 'AgLlH.jpg' ,'AgLlT.jpg'};
%% Induction stimuli:
    stim1 = repmat({'gHlT.jpg'},1,prm.trls.induction/4);
    stim2 = repmat({'gHlH.jpg'},1,prm.trls.induction/4);
    stim3 = repmat({'gTlH.jpg'},1,prm.trls.induction/4);
    stim4 = repmat({'gTlT.jpg'},1,prm.trls.induction/4);
    stimuliGI = Shuffle([stim1 stim2 stim3 stim4]);
    stimuliLI = Shuffle([stim1 stim2 stim3 stim4]);
    
%% Assessment stimuli 
    stim1 = repmat({'AgHlF.jpg'},1,prm.trls.assessment/8);
    stim2 = repmat({'AgHlL.jpg'},1,prm.trls.assessment/8);
    stim3 = repmat({'AgTlF.jpg'},1,prm.trls.assessment/8);
    stim4 = repmat({'AgTlL.jpg'},1,prm.trls.assessment/8);
    stim5 = repmat({'AgFlH.jpg'},1,prm.trls.assessment/8);
    stim6 = repmat({'AgFlT.jpg'},1,prm.trls.assessment/8);
    stim7 = repmat({'AgLlH.jpg'},1,prm.trls.assessment/8);
    stim8 = repmat({'AgLlT.jpg'},1,prm.trls.assessment/8);
    stimuliA1 = Shuffle([stim1 stim2 stim3 stim4 stim5 stim6 stim7 stim8]);
    stimuliA2 = Shuffle([stim1 stim2 stim3 stim4 stim5 stim6 stim7 stim8]);

    
%%
    BgI = struct('subject', repmat(subject{:},1,prm.trls.induction),'block', repmat({'induction'},1,prm.trls.induction),'blocktype', repmat({'global'},1,prm.trls.induction),'stimulus', stimuliGI);
    BlI = struct('subject', repmat(subject{:},1,prm.trls.induction),'block', repmat({'induction'},1,prm.trls.induction),'blocktype', repmat({'local'},1,prm.trls.induction),'stimulus', stimuliLI);
    
%%
    BA1 = struct('subject', repmat(subject{:},1,prm.trls.assessment),'block', repmat({'assessment'},1,prm.trls.assessment),'blocktype', repmat({'global'},1,prm.trls.assessment),'stimulus', stimuliA1);
    BA2 = struct('subject', repmat(subject{:},1,prm.trls.assessment),'block', repmat({'assessment'},1,prm.trls.assessment),'blocktype', repmat({'local'},1,prm.trls.assessment),'stimulus', stimuliA2);
    
%% 
% merge_t : (kirjeldus selle objekti kohta)
    merge_t = [ BgI ,BA1 , BlI, BA2]; %global induction, assessment, local induction, assessment
    merge_t = [ BlI ,BA1 , BgI, BA2]; %local induction, assessment, global induction, assessment
    [merge_t(1:length(merge_t)).date] = deal({datestr(datetime('today'))});

%%
% data file for keeping track of self-report mood
dataslf = struct('subject',subject{:});

%% TRIALITE JOOKSUTAMINE %%

    % THE OVERALL INTRODUCTION TO THE EXPERIMENT
%     picmat = imread(char(strcat(prm.loc.intros, 'GenIntro.jpg')));
%     picstim = Screen('MakeTexture',w, picmat);
%     Screen('DrawTexture', w, picstim, [], [],0); %Screen(�DrawTexture�, windowPointer, texturePointer [,sourceRect] [,destinationRect] [,rotationAngle] 
%     Screen('Flip', w);
%     WaitResp(prm.respDevice{1});
    ShowText(txt.intro); WaitResp('mouse');     
    % Itroduction self-report scales (mood)
    ShowText(txt.scalePractice); WaitResp('mouse');                                
    getScale('HARJUTAMISEKS: Kui positiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    getScale('HARJUTAMISEKS: Kui negatiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});

    % First self-report measures
    ShowText(txt.scaleReal); WaitResp('mouse');                                 
    [slfrepPos, tRespPos] = getScale('Kui positiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    [slfrepNeg, tRespNeg] = getScale('Kui negatiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    dataslf(1).pos1 = slfrepPos;
    dataslf(1).neg1 = slfrepNeg;
    dataslf(1).pos1time = tRespPos;
    dataslf(1).neg1time = tRespNeg;
    
    
    % PRACTICE TRIALS
    % Three types of tasks
    % 1) Respond according to the local shape (induction task)
    % 2) Respond according to the global shape (induction task)
    % 3) Respond according to the letter in the stimuli (H or T; assessment
    % task)
    
    % 1) PRACTICE: induction - local
    if (prm.mapping == 1)
        picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro1.jpg')));
    else
        picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro2.jpg')));
    end
    picstim = Screen('MakeTexture',w, picmat);
    Screen('DrawTexture', w, picstim, [], [],0);
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    [trl] = trialsInBlock('induction',  prm.trls.inductionPractice, stimuliInduction, 'local');
    presentTrials(trl);
    
    % 2) PRACTICE: induction - global
    if (prm.mapping == 1)
        picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro1.jpg')));
    else
        picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro2.jpg')));
    end
    picstim = Screen('MakeTexture',w, picmat);
    Screen('DrawTexture', w, picstim, [], [],0);
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    [trl] = trialsInBlock('induction',  prm.trls.inductionPractice, stimuliInduction, 'global');
    presentTrials(trl);
    
    % 3) PRACTICE: assessment
    if (prm.mapping == 1)
        picmat = imread(char(strcat(prm.loc.intros, 'Aintro1.jpg')));
    else
        picmat = imread(char(strcat(prm.loc.intros, 'Aintro2.jpg')));
    end
    picstim = Screen('MakeTexture',w, picmat);
    Screen('DrawTexture', w, picstim, [], [],0); 
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    [trl] = trialsInBlock('assessment', prm.trls.assessmentPractice, stimuliAssessment, 'assessment1');      
    presentTrials(trl);
     
    
    % THE REAL TRIALS

    % I STAGE - ASSESSMENT
    if (prm.mapping == 1)
        ShowText(txt.Iblock.start1); WaitResp('mouse'); 
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro1.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.assessment ,'center', 'center', col.txt);
    else
        ShowText(txt.Iblock.start2); WaitResp('mouse'); 
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro2.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.assessment1 ,'center', 'center', col.txt);
    end
    %picstim = Screen('MakeTexture',w, picmat);
    %Screen('DrawTexture', w, picstim, [], [],0); 
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    
    [trl] = trialsInBlock('assessment', prm.trls.assessment, stimuliAssessment, 'assessment1');      
    [trials1] = presentTrials(trl);
    
    %I block ends
    Screen('FillRect',w, col.bkg);
    result = num2str(mean([trials1.time]));
    errors = num2str(sum(([trials1.correctRT] == [trials1.firstRT]) == 0));
    DrawFormattedText(w, [txt.Iblock.end result '\nVigade arv: ' errors],...
            'center', cy-(cy/2), col.txt);  
    Screen('Flip', w);    
    WaitResp(prm.respDevice{1})
    
    % II STAGE - INDUCTION
    if prm.order == 1
        if (prm.mapping == 1)
            %picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro1.jpg')));
            ShowText(txt.IIblock.start1); WaitResp('mouse'); 
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.global ,'center', 'center', col.txt);
        else
            %picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro2.jpg')));
            ShowText(txt.IIblock.start2); WaitResp('mouse'); 
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.global1 ,'center', 'center', col.txt);
        end
        %picstim = Screen('MakeTexture',w, picmat);
        %Screen('DrawTexture', w, picstim, [], [],0); 
        Screen('Flip', w);
        WaitResp(prm.respDevice{1});    
        [trl] = trialsInBlock('induction',  prm.trls.induction, stimuliInduction, 'global');
        %Induction trailite jooksutamine:
        [trials2] = presentTrials(trl);
    else
        if (prm.mapping == 1)
            ShowText(txt.IIblock.start1); WaitResp('mouse'); 
            %picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro1.jpg')));
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.local ,'center', 'center', col.txt);
        else
            %picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro2.jpg')));
            ShowText(txt.IIblock.start2); WaitResp('mouse'); 
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.local1 ,'center', 'center', col.txt);
        end
        %picstim = Screen('MakeTexture',w, picmat);
        %Screen('DrawTexture', w, picstim, [], [],0); 
        Screen('Flip', w);
        WaitResp(prm.respDevice{1});
        [trl] = trialsInBlock('induction',  prm.trls.induction, stimuliInduction, 'local');
        %Induction trailite jooksutamine:
        [trials2] = presentTrials(trl);

    end
    
    
    % ASSESSMENT
    if (prm.mapping == 1)
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro1.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.local ,'center', 'center', col.txt);
    else
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro2.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.local1 ,'center', 'center', col.txt);
    end
    %picstim = Screen('MakeTexture',w, picmat);
    %Screen('DrawTexture', w, picstim, [], [],0); 
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    
    [trl] = trialsInBlock('assessment', prm.trls.assessment, stimuliAssessment, 'assessment2');
    %Induction trailite jooksutamine:        
    [trials3] = presentTrials(trl);
    
    % Self-report mood (the end of block I)
    [slfrepPos, tRespPos] = getScale('Kui positiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    [slfrepNeg, tRespNeg] = getScale('Kui negatiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    dataslf(1).pos2 = slfrepPos;
    dataslf(1).neg2 = slfrepNeg;
    dataslf(1).pos2time = tRespPos;
    dataslf(1).neg2time = tRespNeg;
    
    %II block ends - feedback
    Screen('FillRect',w, col.bkg);
    result = num2str(mean([trials2.time trials3.time]));
    errors = num2str(sum([([trials2.correctRT] == [trials2.firstRT]),([trials3.correctRT] == [trials3.firstRT])] == 0));
    DrawFormattedText(w, [txt.IIblock.end result '\nVigade arv: ' errors],...
            'center', cy-(cy/2), col.txt);  
    Screen('Flip', w);    
    WaitResp(prm.respDevice{1})
    

    
    %PAUS
    ShowText(txt.IIblock.start1); WaitResp('mouse'); 
     
    
    % III BLOCK
    % INDUCTION
    if prm.order == 1
        if (prm.mapping == 1)
            ShowText(txt.IIIblock.start1); WaitResp('mouse'); 
            %picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro1.jpg')));
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.local ,'center', 'center', col.txt);            
        else
            ShowText(txt.IIIblock.start2); WaitResp('mouse'); 
            %picmat = imread(char(strcat(prm.loc.intros, 'IlocalIntro2.jpg')));
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.local1 ,'center', 'center', col.txt);
        end
        %picstim = Screen('MakeTexture',w, picmat);
        %Screen('DrawTexture', w, picstim, [], [],0); 
        Screen('Flip', w);
        WaitResp(prm.respDevice{1});
        [trl] = trialsInBlock('induction',  prm.trls.induction, stimuliInduction, 'local');
        %Induction trailite jooksutamine:
        [trials4] = presentTrials(trl);
    else
        if (prm.mapping == 1)
            %picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro1.jpg')));
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.global ,'center', 'center', col.txt);
        else
            %picmat = imread(char(strcat(prm.loc.intros, 'IglobalIntro2.jpg')));
            Screen('FillRect',w, col.bkg);
            DrawFormattedText(w, txt.global1 ,'center', 'center', col.txt);
        end
        %picstim = Screen('MakeTexture',w, picmat);
        %Screen('DrawTexture', w, picstim, [], [],0); 
        Screen('Flip', w);
        WaitResp(prm.respDevice{1});   
        [trl] = trialsInBlock('induction',  prm.trls.induction, stimuliInduction, 'global');
        [trials4] = presentTrials(trl);
    end
    
    
    % V STAGE - ASSESSMENT
    if (prm.mapping == 1)
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro1.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.assessment ,'center', 'center', col.txt);
    else
        %picmat = imread(char(strcat(prm.loc.intros, 'Aintro2.jpg')));
        Screen('FillRect',w, col.bkg);
        DrawFormattedText(w, txt.assessment1 ,'center', 'center', col.txt);
    end
%    picstim = Screen('MakeTexture',w, picmat);
%    Screen('DrawTexture', w, picstim, [], [],0); 
    Screen('FillRect',w, col.bkg);
    DrawFormattedText(w, txt.assessment ,'center', 'center', col.txt);
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    
    [trl] = trialsInBlock('assessment', prm.trls.assessment, stimuliAssessment, 'assessment3');
    %Induction trailite jooksutamine:        
    [trials5] = presentTrials(trl);
    
    
    % Self-report mood (the end of block II)
    [slfrepPos, tRespPos] = getScale('Kui positiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    [slfrepNeg, tRespNeg] = getScale('Kui negatiivselt sa ennast tunned?', {'�ldse mitte' 'V�ga palju'});
    dataslf(1).pos3 = slfrepPos;
    dataslf(1).neg3 = slfrepNeg;
    dataslf(1).pos3time = tRespPos;
    dataslf(1).neg3time = tRespNeg;

    %III block ends - Feedback 
    Screen('FillRect',w, col.bkg);
    result = num2str(mean([trials4.time trials5.time]));
    errors = num2str(sum([([trials4.correctRT] == [trials4.firstRT]),([trials5.correctRT] == [trials5.firstRT])] == 0));
    DrawFormattedText(w, [txt.Iblock.end result '\nVigade arv: ' errors],...
            'center', cy-(cy/2), col.txt);  
    Screen('Flip', w);    
    WaitResp(prm.respDevice{1})
    
    merge_t = [trials1, trials2, trials3, trials4, trials5];

%% L�PETAMINE %% 
    
    Screen('FillRect',w, col.bkg);
    DrawFormattedText(w, txt.end,...
            'center', 'center', col.txt);   
    Screen('Flip', w);
    WaitResp(prm.respDevice{1});
    % Wait for a keyboard button press to exit
    %KbStrokeWait;
    writeData(dataFile,merge_t);  
    writeData(dataFileSlf,dataslf);   

    % Clear the screen. "sca" is short hand for "Screen CloseAll". This clears
    % all features related to PTB. Note: we leave the variables in the
    % workspace so you can have a look at them.
    sca;

%% FUNKTSIOONID %%   
    function [tt] = trialsInBlock(blockName, nroftraisl, stimuli,blocktype)
     
        ss = {};
        if strcmp(blockName, 'induction')
            for k=1:nroftraisl/4
                ss = [ss Shuffle(stimuli)];
            end
        elseif strcmp(blockName, 'assessment')
             for k=1:nroftraisl/8
                ss = [ss Shuffle(stimuli)];
             end
        end
        
        tt = struct('subject', subject{:},'block', repmat({blockName},1,nroftraisl),'blocktype', repmat({blocktype},1,nroftraisl),'stimulus', ss);

 
        for t = 1:length(ss)
            tt(t).ID = t;
            if strcmp(blockName, 'induction')
                picmat = imread(char(strcat(prm.loc.induction,tt(t).stimulus)));
                tt(t).stim = Screen('MakeTexture',w, picmat);
            elseif strcmp(blockName, 'assessment')
                picmat = imread(char(strcat(prm.loc.assessment,tt(t).stimulus)));
                tt(t).stim = Screen('MakeTexture',w, picmat);
            end
        end

        [tt(1:length(tt)).date] = deal({datestr(datetime('now'))});
        
        % Right responses
        if strcmp(blocktype, 'local')
            for t = 1:length(tt)
                if (strcmp(tt(t).stimulus(4), 'T')) && (prm.mapping == 1) 
                    tt(t).correct = '1';
                elseif ~(strcmp(tt(t).stimulus(4), 'T')) && (prm.mapping == 1) 
                    tt(t).correct = '0';
                elseif (strcmp(tt(t).stimulus(4), 'T'))  && (prm.mapping == 2) 
                    tt(t).correct = '0';
                elseif ~(strcmp(tt(t).stimulus(4), 'T')) && (prm.mapping == 2) 
                    tt(t).correct = '1';
                end
            end
        elseif strcmp(blocktype, 'global')
            for t = 1:length(tt)
                if (strcmp(tt(t).stimulus(2), 'T'))  && (prm.mapping == 1) 
                    tt(t).correct = '1';
                elseif ~(strcmp(tt(t).stimulus(2), 'T'))  && (prm.mapping == 1) 
                    tt(t).correct = '0';
                elseif (strcmp(tt(t).stimulus(2), 'T'))  && (prm.mapping == 2) 
                    tt(t).correct = '0';
                elseif ~(strcmp(tt(t).stimulus(2), 'T'))  && (prm.mapping == 2) 
                    tt(t).correct = '1';
                end
            end
        else
            for t = 1:length(tt)
                if (ismember('T',tt(t).stimulus)) && (prm.mapping == 1) 
                    tt(t).correct = '1';
                elseif ~(ismember( 'T',tt(t).stimulus)) && (prm.mapping == 1) 
                    tt(t).correct = '0';
                elseif (ismember( 'T',tt(t).stimulus)) && (prm.mapping == 2) 
                    tt(t).correct = '0';
                elseif ~(ismember( 'T',tt(t).stimulus)) && (prm.mapping == 2) 
                    tt(t).correct = '1';
                end
            end
        end
    end


    function [trls] = presentTrials(trls)
        
        % Globaalses �lesande juhend:
%         Screen('FillRect',w, col.bkg);
%         DrawFormattedText(w, text,'center', 'center', col.txt);   
%         Screen('Flip', w);
%         WaitResp(prm.respDevice{1});
        
        %Alustan trialite jooksutamist
        for i=1:length(trls)
            %1. Fiksatsioonirist
            Screen('DrawTexture', w, stim.fix);
            Screen('Flip', w);
            WaitSecs(prm.fix);                                

            % �lesanne
            Screen('DrawTexture', w, trls(i).stim, [], [],0); 
            tStim = Screen('Flip', w);
            %[rt, resp] = WaitResp(prm.respDevice{1});
            % RESPONSE %
            err = 'mistake'; attempt = 0;
            while strcmp(err,'mistake')
                [tResp, key] = WaitResp(prm.respDevice{1});                             % oota vastust
                trls(i).correctRT = tResp - tStim;                                    % igal ringil kirjutab selle �le
                if attempt == 0;
                    trls(i).firstRT = tResp - tStim;                                  % esimesel ringil salvestab eraldi esimese vastuse aja
                end
                % feedback %
                resp = find([prm.respDevice{2:3}]==find(key));                          % kumb vastus (1 - vasak, 2 - parem)
                if ~any(resp)  %~ not                                                         % kui oli vale nupuvajutus, oota edasi
                    continue
                end
%                 if strcmp(trls(i).correct, '1') && true(key(3))
%                     err = 'correct';
%                 elseif strcmp(trls(i).correct, '0') && false(key(3)) 
%                      err = 'correct';
%                 end
                if strcmp(trls(i).correct, '1') && resp == 1
                    err = 'correct';
                elseif strcmp(trls(i).correct, '0') && resp == 2
                     err = 'correct';
                end
                attempt = attempt + 1;  
                if find(key)==KbName('ESCAPE') % kui vajutatakse esc'i, siis j�ta pooleli
                    sca
                    close all
                    return
                end
           end
            
            %
            trls(i).time = tResp - tStim;
            trls(i).response = key;

            % Triali l�pp
            Screen('FillRect',w, col.bkg);
            Screen('Flip', w);
            WaitSecs(prm.ITI);
        end
    end



    function [time, key] = WaitResp(respDevice, maxTime)
        
        respOptions = {logical([1 0 0]) logical([0 0 1])};
        
        if prm.testRun==1
            time = GetSecs;
            key = randi(length(respOptions));
            key = respOptions{key};
            return
        end
        
        if nargin < 1
            respDevice = 'key';
        end
        
        if nargin < 2 % kui ooteaega pole m��ratud, oota l�putult (100 min)
            maxTime = 600000;
        end
        
        key = []; % vastuselugeja nullimine
        tic; % ooteaja k�ivitamine
        while toc < maxTime % kuni ooteaeg pole t�is
            if strcmp(respDevice , 'mouse') % kontroli hiirt
                
                [x,y,key] = GetMouse;
                if any(key([1 3])==1) % kui on antud vastus, v�lju funktsioonist
                    time = GetSecs;
                    state = key;
                    while any(state) % wait for release
                        [x,y,state] = GetMouse;
                    end
                    return
                end
            elseif strcmp(respDevice, 'key') % kontrolli klaviatuuri
                
                [resp, secs, key] = KbCheck;
                if any(resp) % kui on antud vastus, v�lju funktsioonist
                    time = GetSecs;
                    state = resp;
                    while any(state) % wait for release
                        state = KbCheck;
                    end
                    if find(key)== KbName('ESCAPE') % kui vajutatakse esc'i, siis j�ta pooleli
                        sca
                        close all
                        return
                    end
                    return
                end
                
            end
            [resp, secs, key] = KbCheck; % kui vajutatakse esc'i, siis j�ta pooleli
            if find(key)==KbName('ESCAPE') 
                sca
                close all
                return
            end 
        end        
    end

    function writeData(dataFile, data)                                          % tstruktuuri failist cell tekstifaili 
        
        % kui datafaili veel pole, tr�kib sinna tulpade nimed
        if ~exist(dataFile,'file')
            cell2dlm(dataFile,fields(data)', '\t' , 'a');
        end

        table = squeeze(struct2cell(data))';

        if any(strcmp('trial', fields(data)))                                   % kui on vastav fail, j�tab v�lja esitamata trialid
            rows2export = [];                                                       
            for row = 1:size(table,1)                               
                if ~isempty(table{row,strmatch('trial',fields(data),'exact')})    
                    rows2export(end+1) = row;
                end
            end
            table = table(rows2export,:);
        end
        
        cell2dlm(dataFile,table, '\t' , 'a');
    end

    function cell2dlm(outFile,inCell,delimiter,append)                          % adapted from (c) Roland Pfister %
        outF = fopen(outFile,append);                                       % Open output file and prepare output array.
        outA = cell(size(inCell,1),size(inCell,2));
        for i = 1:size(inCell,1)                                            % Evaluate and write input array.
            for j = 1:size(inCell,2)
                % convert to strings
                if numel(inCell{i,j}) == 0                                  % empty cells 
                    outA{i,j} = '';
                elseif isnumeric(inCell{i,j}) || islogical(inCell{i,j})     % numbers to strings
                    outA{i,j} = num2str(inCell{i,j}(1,1));
                elseif iscell(inCell{i,j})                                  % nested cell to numbers or txt (works only on one hierarchical level).
                    if size(inCell{i,j},1) == 1 && size(inCell{i,j},1) == 1
                        if isnumeric(inCell{i,j}{1,1})
                            outA{i,j} = num2str(inCell{i,j}{1,1}(1,1));
                        elseif ischar(inCell{i,j}{1,1})
                             outA{i,j} = inCell{i,j}{1,1};
                        end
                    end
                elseif ischar(inCell{i,j})                                 % keep strings
                     outA{i,j} = inCell{i,j};
                end
                fprintf(outF,'%s',outA{i,j});                              % Write outA to outFile
                if j ~= size(inCell,2)                                     % delimiter is appended for all but the last element of each row
                    fprintf(outF,delimiter);
                end
            end
            fprintf(outF,'\r\n');                                           % At the end of a row, a newline is written to the outA outFile.
        end
        fclose(outF);
    end

    function [response, RT] = getScale(question, scaleLabels, showScore)        % visuaal-analoog skaala
       
        % TELJE PARAMEETRID %
        step = round(rect(3)/(20+2));                                           % ekraani suurusest l�htumine
        gridRect = [cx-8*step cy-5*step cx+8*step cy+5*step];                   % raam
        
        Screen('TextColor', w, col.obj);                                        % teksti omadused
        Screen('TextFont', w, prm.txtFont);                     
        Screen('TextSize', w, round(prm.txtSize*0.75)); 
        rightBound = Screen('TextBounds', w, scaleLabels{2}, cx, cy, 0);
        
        rad = round(rect(4)/100);                                               % kursor
        circM = Circle(rad)+col.bkg(1) + Circle(rad)*(col.obj(1)-col.bkg(1)-1);
        circ = Screen('MakeTexture',w, circM);
        
        % ESITA JA OOTA VASTUST 
        resp = [];
        Screen('FillRect', w, col.bkg);
        SetMouse(cx, cy, w);                                                    % kursor keskele
        showTime = GetSecs;                                                     % k�simuse esitusaeg
        while ~any(resp)
            CheckStop;
            % telg %
            Screen('DrawLine', w, col.mid, gridRect(1), cy, gridRect(3), cy ,4);
            % nooleotsad %
            Screen('FillPoly', w, col.mid, [gridRect(1),cy-step/2; gridRect(1),cy+step/2; gridRect(1)-step/2,cy]);
            Screen('FillPoly', w, col.mid, [gridRect(3),cy-step/2; gridRect(3),cy+step/2; gridRect(3)+step/2,cy]);
            % sildid %
            Screen('TextSize', w, prm.txtSize);                                 % teksti font        
            DrawFormattedText(w, question, 'center', gridRect(2), col.obj, 74, [], [], 2);
            Screen('TextSize', w, round(prm.txtSize*0.75));                     % teksti font        
            Screen('DrawText', w, scaleLabels{1}, gridRect(1), cy+3*prm.txtSize, col.obj);                        % �lemisd labelid
            Screen('DrawText', w, scaleLabels{2}, gridRect(3)-rightBound(3), cy+3*prm.txtSize, col.obj);        
            % tagasiside %
            [x,y,resp] = GetMouse;
            % incr = GetMouseWheel; % see on ainult macil
            if x < gridRect(1) 
                x = gridRect(1);
            elseif x > gridRect(3); 
                x = gridRect(3);
            end
            SetMouse(x, cy, w);                                                 % hiire hoidmine teljel
            
            if exist('showScore')
                Screen('DrawText', w, num2str(round(((x-gridRect(1))/(gridRect(3)-gridRect(1)))*366)+200), x-prm.txtSize, cy-2*prm.txtSize, col.obj);
            end
            
            Screen('DrawTexture', w, circ, [], [x-rad cy-rad x+rad-1 cy+rad-1], [], [], 0);
            Screen('Flip', w);
        end
        
        RT = GetSecs-showTime;                                                  % response time
        response = ((x-gridRect(1))/(gridRect(3)-gridRect(1)))*100;             % normalize response % 0:100
        
        while any(resp)                                                         % wait for mouse release
            [x,y,resp] = GetMouse;
        end

    end
    function CheckStop()
        [resp, secs, key] = KbCheck;                                            % kui vajutatakse esc'i, siis j�ta pooleli
        [x,y,mouse] = GetMouse;
        if find(key)==KbName('ESCAPE')
            sca
            close all
            return
        end 
%         if find(mouse)==2                                                       % kui on vajutatud keskmist nuppu
%             while any(mouse)                                                    % oota nupu vabastamist
%                 [x,y,mouse] = GetMouse;
%             end            
%             ShowText(txt.pause); WaitResp('mouse');                             % n�ita teksti ja oota j�tkamist
%             ShowText(txt.waitEEG); WaitResp('key');     
%             ShowText(txt.start); WaitResp('mouse');                                 
%         end
    end
    function time = ShowText(txt, colorScheme, txtSize, align)                  % teksti n�itamine, v�ljastab aja
        
        if nargin <2
            colorScheme = {col.txt col.bkg};
        end

        if nargin >= 3 % kui fondisuurus on m��ratud
            Screen('TextSize', w, round(txtSize)); % fondi suurus
        else
            Screen('TextSize', w, prm.txtSize); % fondi suurus            
        end
        
        if nargin >= 4
            xalign = align;
        else
            xalign = 'center';
        end
            
        Screen('TextColor', w, colorScheme{1}); % teksti v�rv
        Screen('TextFont', w, prm.txtFont); % teksti font            
        Screen('FillRect', w, colorScheme{2}); % tausta v�rv
        DrawFormattedText(w, txt,xalign,'center',[], 74, [], [], 2);  
        time = Screen('Flip', w);
    end

end