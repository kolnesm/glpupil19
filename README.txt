##########################################################
resolution: 1200 x 800
viewing distance from the screen: 75 cm
stimulus size: global 60 x 45 mm; local 6 X 4.5 mm
visual angle calculator(http://elvers.us/perception/visualAngle/)

##########################################################
Parameters at the beginning of the script:
- trials in the assessment block (prm.trialsInduction)
- trials in the induction block (prm.trialsAssessment)
- inter-trial-interval (1 second)
- fixation cross period (0.5 second)
- response mapping
- background and stimuli color (ei ole veel lisatud)

#########################################################
Number of trials in the different blocks:
Navon indcution and measurement task
Practice trials:
Navon indcution - 8 trials & 8 trials
Navon measurement - 8 trials

Real trials
Navon measurement - 40 trials

Navon induciton - 100 trials
Navon measurement - 40 trials

Navon induciton - 100 trials
Navon measurement - 40 trials

